import { useState } from "react";
import { Route, Routes, useNavigate } from "react-router";
import "./app.css";
import Home from "./pages/home/Home";
import Login from "./pages/login/Login";
import Register from "./pages/login/Register";

function App() {
  const [login, setLog] = useState(false);
  const navigate = useNavigate();

  const handleLogin = (e) => {
    e.preventDefault();
    setLog(true);
    navigate("/");
  };

  const setLogin = () => {
    setLog(false);
    navigate("/login");
  };

  return (
    <div className="app">
      <Routes>
        <Route path="/" element={<Home login={login} />} />
        <Route
          path="/login"
          element={
            <Login
              login={login}
              handleLogin={handleLogin}
              setLogin={setLogin}
            />
          }
        />
        <Route path="/register" element={<Register />} />
      </Routes>
    </div>
  );
}

export default App;
