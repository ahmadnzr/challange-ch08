import "./login.css";
import { Button, Form } from "react-bootstrap";
import { useState } from "react";

const Login = ({ user, handleLogin }) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState([]);

  const err = [];

  return (
    <div className="login d-flex align-items-center justify-content-center">
      <div className="form-card d-flex flex-column align-items-center justify-content-around">
        <h2>LOGIN</h2>
        <ul>
          {error.map((err) => (
            <li>{err}</li>
          ))}
        </ul>
        <Form
          className="d-flex flex-column justify-content-center"
          onSubmit={handleLogin}
        >
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Control
              type="text"
              placeholder="Input your username"
              onChange={(e) => setUsername(e.target.value)}
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Control
              type="password"
              placeholder="Input your password"
              onChange={(e) => setPassword(e.target.value)}
            />
          </Form.Group>

          <Button variant="primary" type="submit">
            Login
          </Button>
        </Form>
      </div>
    </div>
  );
};

export default Login;
