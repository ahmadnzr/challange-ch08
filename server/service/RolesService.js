"use strict";

/**
 * Create a Role
 * # Create a Role  Use this endpoint to create a role. when you create an user, default role is `player`.  Only admin can do this
 *
 * body Object Payload to create new Role
 * returns RoleEntity
 **/
exports.createRole = function (body) {
  return new Promise(function (resolve, reject) {
    var examples = {};
    examples["application/json"] = {
      name: "PLAYER",
      id: 1,
      createAt: "2021-12-07T01:35:38+00:00",
      updatedAt: "2021-12-07T01:35:38+00:00",
    };
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
};

/**
 * Update a Role
 * # Update Role  Use this enpoint to update Role
 *
 * id BigDecimal The id of Role
 * returns Object
 **/
exports.deleteRole = function (id) {
  return new Promise(function (resolve, reject) {
    var examples = {};
    examples["application/json"] = {};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
};

/**
 * Get all Roles
 * # Get All existing Roles  Use this endpoint to get all Roles. Only `Admin` can do this
 *
 * returns ArrayOfRoles
 **/
exports.getRoles = function () {
  return new Promise(function (resolve, reject) {
    var examples = {};
    examples["application/json"] = [
      {
        id: 1,
        name: "PLAYER",
      },
      {
        id: 2,
        name: "Admin",
      },
    ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
};

/**
 * Update a Role
 * # Update Role  Use this enpoint to update Role
 *
 * body Object Payload to create new Role
 * id BigDecimal The id of Role
 * returns RoleEntity
 **/
exports.updateRole = function (body, id) {
  return new Promise(function (resolve, reject) {
    var examples = {};
    examples["application/json"] = {
      name: "PLAYER",
      id: 1,
      createAt: "2021-12-07T01:35:38+00:00",
      updatedAt: "2021-12-07T01:35:38+00:00",
    };
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
};
