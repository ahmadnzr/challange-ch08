"use strict";

/**
 * Create an User
 *  # Create New User  Use this endpoint to create new user. username must be **unique**
 *
 * body Object Payload to create new User (optional)
 * returns UserEntity
 **/
exports.createUser = function (body) {
  return new Promise(function (resolve, reject) {
    var examples = {};
    examples["application/json"] = {
      role: "PLAYER",
      id: "6268e31f-cf4e-41b7-af96-3f0aee7d00b4",
      username: "naruto",
    };
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
};

/**
 * Create User Biodata
 * # Create User Bidoata  Use this enpoint to create user Biodata.
 *
 * body Object Payload to create user biodata (optional)
 * id String The id of an User
 * returns BiodataEntity
 **/
exports.createUserBiodata = function (body, id) {
  return new Promise(function (resolve, reject) {
    var examples = {};
    examples["application/json"] = {
      createdAt: "createdAt",
      phoneNumber: "phoneNumber",
      address: "address",
      avatarUrl: "avatarUrl",
      bio: "Tidak menarik kata-kata adalah jalan ninjaku",
      fullname: "Naruto Uzumaki",
      updatedAt: "updatedAt",
    };
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
};

/**
 * Delete an User
 * # Delete User  Use this enpoint to delete User.
 *
 * id String The id of an User
 * returns Object
 **/
exports.deleteUser = function (id) {
  return new Promise(function (resolve, reject) {
    var examples = {};
    examples["application/json"] = {};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
};

/**
 * Get an User
 * # Get all User  Use this enpoint to get an User
 *
 * id String The id of an User
 * returns UserEntity
 **/
exports.getAnUser = function (id) {
  return new Promise(function (resolve, reject) {
    var examples = {};
    examples["application/json"] = {
      role: "PLAYER",
      id: "6268e31f-cf4e-41b7-af96-3f0aee7d00b4",
      username: "naruto",
    };
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
};

/**
 * Get User Biodata
 * # Get User Bidoata  Use this enpoint to get user Biodata. When user register, bidoata is null
 *
 * id String The id of an User
 * returns BiodataEntity
 **/
exports.getUserBiodata = function (id) {
  return new Promise(function (resolve, reject) {
    var examples = {};
    examples["application/json"] = {
      createdAt: "createdAt",
      phoneNumber: "phoneNumber",
      address: "address",
      avatarUrl: "avatarUrl",
      bio: "Tidak menarik kata-kata adalah jalan ninjaku",
      fullname: "Naruto Uzumaki",
      updatedAt: "updatedAt",
    };
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
};

/**
 * Get User Games
 * # Get User Games  Use this enpoint to get user games(room)
 *
 * id String The id of an User
 * status String The status of the user games(room) (optional)
 * returns ArrayOfRooms
 **/
exports.getUserGames = function (id, status) {
  return new Promise(function (resolve, reject) {
    var examples = {};
    examples["application/json"] = [
      {
        id: "6268e31f-cf4e-41b7-af96-3f0aee7d10b3",
        name: "Kepakkan sayap kebhinekaan",
        status: "ACTIVE",
        players: [
          {
            id: "",
            username: "naruto",
            type: "PLAYER_1",
          },
          {
            id: "",
            username: "sasuke",
            type: "PLAYER_2",
          },
        ],
        createAt: "2021-12-07T01:35:38+00:00",
        updatedAt: "2021-12-07T01:35:38+00:00",
      },
      {
        id: "6268e31f-cf4e-41b7-XXXX-3f0aee-7d10b3",
        name: "Kepakkan sayap kebhinekaan",
        status: "INACTIVE",
        winner: "PLAYER_2",
        players: [
          {
            id: "",
            username: "naruto",
            type: "PLAYER_1",
          },
          {
            id: "",
            username: "sasuke",
            type: "PLAYER_2",
          },
        ],
        createAt: "2021-12-07T01:35:38+00:00",
        updatedAt: "2021-12-07T01:35:38+00:00",
      },
    ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
};

/**
 * Get User Round
 * # Get User Round  Use this enpoint to get user round
 *
 * id String The id of an User
 * status String The status of the user round (optional)
 * returns GameRoundsEntity
 **/
exports.getUserRound = function (id, status) {
  return new Promise(function (resolve, reject) {
    var examples = {};
    examples["application/json"] = [
      {
        id: 1,
        name: "ROUND_1",
        status: "FINISHED",
        winnerId: "",
        roomId: "",
        createdAt: "",
        updatedAt: "",
      },
      {
        id: 2,
        name: "ROUND_2",
        status: "PLYAER_2_RETURN",
        roomId: "",
        createdAt: "",
        updatedAt: "",
      },
    ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
};

/**
 * Get User Stats
 * # Get User Stats  Use this enpoint to get user Stats(`Point, Level, Win Rate`)
 *
 * id String The id of an User
 * returns StateEntity
 **/
exports.getUserStats = function (id) {
  return new Promise(function (resolve, reject) {
    var examples = {};
    examples["application/json"] = {
      level: "CLASS_A",
      winRate: 10,
      point: 700,
      player: {
        name: "naruto",
        id: "",
      },
    };
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
};

/**
 * Get all User
 *  # Get all Users  Use this endpoint to get All Users. Only `Admin` can do this
 *
 * returns ArrayOfUsers
 **/
exports.getUsers = function () {
  return new Promise(function (resolve, reject) {
    var examples = {};
    examples["application/json"] = [
      {
        id: "6268e31f-cf4e-41b7-af96-3f0aee7d00b4",
        username: "naruto",
        role: "PLAYER",
      },
      {
        id: "588dab7c-52c9-4900-b6b8-61c9aadbcad2",
        username: "sasuke",
        role: "PLAYER",
      },
    ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
};

/**
 * Update an User
 * # Update User  Use this enpoint to update User.
 *
 * body Object Payload to create new User (optional)
 * id String The id of an User
 * returns UserEntity
 **/
exports.updateUser = function (body, id) {
  return new Promise(function (resolve, reject) {
    var examples = {};
    examples["application/json"] = {
      role: "PLAYER",
      id: "6268e31f-cf4e-41b7-af96-3f0aee7d00b4",
      username: "naruto",
    };
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
};

/**
 * Update User Biodata
 * # Update User Bidoata  Use this enpoint to update user Biodata.
 *
 * body Object Payload to create user biodata (optional)
 * id String The id of an User
 * returns BiodataEntity
 **/
exports.updateUserBiodata = function (body, id) {
  return new Promise(function (resolve, reject) {
    var examples = {};
    examples["application/json"] = {
      createdAt: "createdAt",
      phoneNumber: "phoneNumber",
      address: "address",
      avatarUrl: "avatarUrl",
      bio: "Tidak menarik kata-kata adalah jalan ninjaku",
      fullname: "Naruto Uzumaki",
      updatedAt: "updatedAt",
    };
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
};
