"use strict";

/**
 * Fight the Player
 * # Fight with User  Use this endpoint to challenge other players
 *
 * body Object Payload to create new Room
 * id String The id of an User
 * returns ChallengeUserEntity
 **/
exports.fightPlayer = function (body, id) {
  return new Promise(function (resolve, reject) {
    var examples = {};
    examples["application/json"] = {
      players: [
        {
          id: "6268e31f-cf4e-41b7-af96-3f0aee7d00b4",
          username: "naruto",
          type: "PLAYER_1",
        },
        {
          id: "588dab7c-52c9-4900-b6b8-61c9aadbcad2",
          username: "sasuke",
          type: "PLAYER_2",
        },
      ],
      name: "Kepakkan sayap kebhinekaan",
      id: "6268e31f-cf4e-41b7-af96-3f0aee7d10b3",
      status: "ACTIVE",
    };
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
};

/**
 * Play the Game
 * # Play the Game  Use this endpoint to play the game. there are **3 rounds** on the room
 *
 * body Object Payload to play the Game (optional)
 * id String The id of the room
 * returns RoundEntity
 **/
exports.gameFight = function (body, id) {
  return new Promise(function (resolve, reject) {
    var examples = {};
    examples["application/json"] = {
      createdAt: "",
      winnerId: "",
      name: "ROUND_1",
      id: "",
      roomId: "",
      status: "PLAYER_1_TURN",
      updatedAt: "",
      finishedAt: "",
    };
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
};

/**
 * Get Game Rounds
 * # Get Game Rounds  Use this endpoint to see game rounds. **Only `ADMIN` can do this**
 *
 * returns GameRoundsEntity
 **/
exports.getAllRound = function () {
  return new Promise(function (resolve, reject) {
    var examples = {};
    examples["application/json"] = [
      {
        id: 1,
        name: "ROUND_1",
        status: "FINISHED",
        winnerId: "",
        roomId: "",
        createdAt: "",
        updatedAt: "",
      },
      {
        id: 2,
        name: "ROUND_2",
        status: "PLYAER_2_RETURN",
        roomId: "",
        createdAt: "",
        updatedAt: "",
      },
    ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
};

/**
 * Get Game Option
 * # Get Game Option  Use this endpoint to see game options
 *
 * returns GameOptionEntity
 **/
exports.getGameOption = function () {
  return new Promise(function (resolve, reject) {
    var examples = {};
    examples["application/json"] = [
      {
        id: 1,
        name: "GUNTING",
        superiorId: 2,
        inferiorId: 3,
        createdAt: "",
        updatedAt: "",
      },
      {
        id: 2,
        name: "BATU",
        superiorId: 3,
        inferiorId: 1,
        createdAt: "",
        updatedAt: "",
      },
      {
        id: 3,
        name: "KERTAS",
        superiorId: 1,
        inferiorId: 2,
        createdAt: "",
        updatedAt: "",
      },
    ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
};

/**
 * Join to a Room
 * # Join to a Room  Use this endpoint to join a Room. Ask your challenger for the `room id`
 *
 * id String The id of the room
 * returns RoomEntity
 **/
exports.joinRoom = function (id) {
  return new Promise(function (resolve, reject) {
    var examples = {};
    examples["application/json"] = {
      players: [
        {
          id: "6268e31f-cf4e-41b7-af96-3f0aee7d00b4",
          username: "naruto",
          type: "PLAYER_1",
        },
        {
          id: "588dab7c-52c9-4900-b6b8-61c9aadbcad2",
          username: "sasuke",
          type: "PLAYER_2",
        },
      ],
      name: "Kepakkan sayap kebhinekaan",
      id: "6268e31f-cf4e-41b7-af96-3f0aee7d10b3",
      createAt: "2021-12-07T01:35:38+00:00",
      status: "ACTIVE",
      updatedAt: "2021-12-07T01:35:38+00:00",
    };
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
};
