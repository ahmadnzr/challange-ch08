"use strict";

/**
 * Create Room
 * # Create a Room  Use this endpoint to create a Room for the game
 *
 * body Object Payload to create new Room
 * returns CreatedRoomEntity
 **/
exports.createRoom = function (body) {
  return new Promise(function (resolve, reject) {
    var examples = {};
    examples["application/json"] = {
      players: [
        {
          id: "6268e31f-cf4e-41b7-af96-3f0aee7d00b4",
          username: "naruto",
          type: "PLAYER_1",
        },
      ],
      name: "Kepakkan sayap kebhinekaan",
      id: "6268e31f-cf4e-41b7-af96-3f0aee7d10b3",
      createAt: "2021-12-07T01:35:38+00:00",
      status: "WAITING_FOR_PLAYER_1",
      updatedAt: "2021-12-07T01:35:38+00:00",
    };
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
};

/**
 * Get All Room
 * # Get All Room  Use this endpoint to get All Rooms
 *
 * status String The status of Rooms. `ACTIVE` or `INACTIVE`  `ACTIVE` is mean **the game is open** and `INACTIVE` is mean **the game is over** (optional)
 * returns ArrayOfRooms
 **/
exports.getAllRoom = function (status) {
  return new Promise(function (resolve, reject) {
    var examples = {};
    examples["application/json"] = [
      {
        id: "6268e31f-cf4e-41b7-af96-3f0aee7d10b3",
        name: "Kepakkan sayap kebhinekaan",
        status: "ACTIVE",
        players: [
          {
            id: "",
            username: "naruto",
            type: "PLAYER_1",
          },
          {
            id: "",
            username: "sasuke",
            type: "PLAYER_2",
          },
        ],
        createAt: "2021-12-07T01:35:38+00:00",
        updatedAt: "2021-12-07T01:35:38+00:00",
      },
      {
        id: "6268e31f-cf4e-41b7-XXXX-3f0aee-7d10b3",
        name: "Kepakkan sayap kebhinekaan",
        status: "INACTIVE",
        winner: "PLAYER_2",
        players: [
          {
            id: "",
            username: "naruto",
            type: "PLAYER_1",
          },
          {
            id: "",
            username: "sasuke",
            type: "PLAYER_2",
          },
        ],
        createAt: "2021-12-07T01:35:38+00:00",
        updatedAt: "2021-12-07T01:35:38+00:00",
      },
    ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
};
