"use strict";

/**
 * Create a Level
 * # Create a Level  Use this endpoint to create a Level. only `ADMIN` can do this
 *
 * body Object Payload to create new Level
 * returns LevelEntity
 **/
exports.createLevel = function (body) {
  return new Promise(function (resolve, reject) {
    var examples = {};
    examples["application/json"] = {
      minPoint: 500,
      maxPoint: 1000,
      name: "CLASS A",
      id: 0.8008281904610115,
      createAt: "2021-12-07T01:35:38+00:00",
      updatedAt: "2021-12-07T01:35:38+00:00",
    };
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
};

/**
 * Get all Level
 * # Create a Level  Use this endpoint to get all Level.
 *
 * level String The name of level that you use to seacrh other player (optional)
 * point Integer The number of point that you use to seacrh other player (optional)
 * returns ArrayOfLevels
 **/
exports.getLevels = function (level, point) {
  return new Promise(function (resolve, reject) {
    var examples = {};
    examples["application/json"] = [
      {
        id: "a91804de-c014-4bfe-beb2-7326c91c47f4",
        name: "CLASS_A",
        minPoint: 500,
        maxPoint: 1000,
        totalPlayer: 10,
        createdAt: "",
        updatedAt: "",
      },
      {
        id: "a91804de-c014-4bfe-beb2-7326c91c47f4",
        name: "CLASS_B",
        minPoint: 1000,
        maxPoint: 1500,
        totalPlayer: 15,
        createdAt: "",
        updatedAt: "",
      },
    ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
};

/**
 * Update a Level
 * # Update a Level  Use this endpoint to create a Level. only `ADMIN` can do this
 *
 * body Object Payload to create new Level
 * id String The id of Level
 * returns LevelEntity
 **/
exports.updateLevel = function (body, id) {
  return new Promise(function (resolve, reject) {
    var examples = {};
    examples["application/json"] = {
      minPoint: 500,
      maxPoint: 1000,
      name: "CLASS A",
      id: 0.8008281904610115,
      createAt: "2021-12-07T01:35:38+00:00",
      updatedAt: "2021-12-07T01:35:38+00:00",
    };
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
};
