"use strict";

var utils = require("../utils/writer.js");
var Roles = require("../service/RolesService");

module.exports.createRole = function createRole(req, res, next, body) {
  Roles.createRole(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.deleteRole = function deleteRole(req, res, next, id) {
  Roles.deleteRole(id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getRoles = function getRoles(req, res, next) {
  Roles.getRoles()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.updateRole = function updateRole(req, res, next, body, id) {
  Roles.updateRole(body, id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
