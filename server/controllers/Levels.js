"use strict";

var utils = require("../utils/writer.js");
var Levels = require("../service/LevelsService");

module.exports.createLevel = function createLevel(req, res, next, body) {
  Levels.createLevel(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getLevels = function getLevels(req, res, next, level, point) {
  Levels.getLevels(level, point)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.updateLevel = function updateLevel(req, res, next, body, id) {
  Levels.updateLevel(body, id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
