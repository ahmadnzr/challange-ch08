"use strict";

var utils = require("../utils/writer.js");
var Users = require("../service/UsersService");

module.exports.createUser = function createUser(req, res, next, body) {
  Users.createUser(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.createUserBiodata = function createUserBiodata(
  req,
  res,
  next,
  body,
  id
) {
  Users.createUserBiodata(body, id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.deleteUser = function deleteUser(req, res, next, id) {
  Users.deleteUser(id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getAnUser = function getAnUser(req, res, next, id) {
  Users.getAnUser(id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getUserBiodata = function getUserBiodata(req, res, next, id) {
  Users.getUserBiodata(id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getUserGames = function getUserGames(
  req,
  res,
  next,
  id,
  status
) {
  Users.getUserGames(id, status)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getUserRound = function getUserRound(
  req,
  res,
  next,
  id,
  status
) {
  Users.getUserRound(id, status)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getUserStats = function getUserStats(req, res, next, id) {
  Users.getUserStats(id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getUsers = function getUsers(req, res, next) {
  Users.getUsers()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.updateUser = function updateUser(req, res, next, body, id) {
  Users.updateUser(body, id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.updateUserBiodata = function updateUserBiodata(
  req,
  res,
  next,
  body,
  id
) {
  Users.updateUserBiodata(body, id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
