"use strict";

var utils = require("../utils/writer.js");
var Games = require("../service/GamesService");

module.exports.fightPlayer = function fightPlayer(req, res, next, body, id) {
  Games.fightPlayer(body, id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.gameFight = function gameFight(req, res, next, body, id) {
  Games.gameFight(body, id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getAllRound = function getAllRound(req, res, next) {
  Games.getAllRound()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getGameOption = function getGameOption(req, res, next) {
  Games.getGameOption()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.joinRoom = function joinRoom(req, res, next, id) {
  Games.joinRoom(id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
