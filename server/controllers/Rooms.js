"use strict";

var utils = require("../utils/writer.js");
var Rooms = require("../service/RoomsService");

module.exports.createRoom = function createRoom(req, res, next, body) {
  Rooms.createRoom(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getAllRoom = function getAllRoom(req, res, next, status) {
  Rooms.getAllRoom(status)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
